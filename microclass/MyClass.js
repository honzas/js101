
var MicroClass = require('./MicroClass.js');

function MyClass(value) {
  this.options.value = value;
  this.value = value;
}

MyClass.prototype = Object.create(MicroClass.prototype);
MyClass.prototype.constructor = MyClass;

MyClass.prototype.options = {
  value: null
};
MyClass.prototype.value = null;
MyClass.prototype.getValueObject = function () {
  return this.options.value;
};
MyClass.prototype.getValuePrimitive = function () {
  return this.value;
};
MyClass.prototype.setValue = function (value) {
  this.options.value = value;
  this.value = value;
};

var myInstance1 = new MyClass(1);
var myInstance2 = new MyClass(2);

myInstance1.log('Primite value:', myInstance1.getValuePrimitive());
myInstance1.log('Object value:', myInstance1.getValueObject());

/*
myInstance2.setValue(3);

myInstance1.log('Primite value:', myInstance1.getValuePrimitive());
myInstance1.log('Object value:', myInstance1.getValueObject());
*/

MyClass.prototype.getValueObject = function () {
  return 'Nothing changed';
}

myInstance1.log('NewObject value:', myInstance1.getValueObject());
