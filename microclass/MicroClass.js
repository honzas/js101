
function MicroClass(options) {
  this.options = this.extend(this.extend({}, this.options || {}), options || {});
}

MicroClass.prototype = {
  log: function () {
    var args = Array.prototype.slice.call(arguments);
    args.unshift(this.constructor.name, ':');
    console.log.apply(console, args);
  }
};

module.exports = MicroClass;
