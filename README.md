jak pustit testy:

nejdriv instalovat zavislosti
`npm install`

Potom pustit unit testy v mocha

`./node_modules/mocha/bin/mocha ./tests/unit`

Potom mame akceptacni testy ve firefoxu / chrome / exploreru nebo headless ve phantomjs.
Podle toho co mame za nainstalovane prohlizece.

Dale musime nainstalovat soucastky seleniumserveru (pokud ho uz je nemame)

`./node_modules/selenium-standalone/bin/selenium-standalone install`

a spustit selenium server (nejlepe v jinem okne terminalu)

`./node_modules/selenium-standalone/bin/selenium-standalone start`

Nakonec potrebujeme, aby nase todo apliakce bezela v libovolnem http serveru (nebo zaridte, aby vam bezela jenom z filesystemu - nevim jak).
Abychom si ji mohli prohlednout v prohlizeci - me tohle zaridi sam od sebe phpstorm.
Kazdopadne hotovou url, ze ktere na apliakci koukate, zadejte do souboru
`codecept.json` do `helpers:WebDriverIO:url` misto te moji hodnoty

Ted muzeme spustit akceptacni testy a sledovat ve firefoxu prubeh.

`./node_modules/codeceptjs/bin/codecept.js run --steps`