/**
 * Created by jan on 4/29/16.
 */

var TODO = ( function () {
	function  createItem(text) {
		var li = document.createElement('li');
		var buttonDiv = document.createElement('div');
		var buttonDone = document.createElement('button');
		var buttonCancel = document.createElement('button');

		li.textContent = text;

		buttonDone.textContent = '✔';
		buttonDone.addEventListener('click', markAsDone);
		buttonCancel.textContent = 'X';
		buttonCancel.addEventListener('click', removeItem);

		buttonDiv.appendChild(buttonDone);
		buttonDiv.appendChild(buttonCancel);
		buttonDiv.className = 'button';
		li.appendChild(buttonDiv);
		return li;
	}

	function incrementCounter() {
		var counterElement = document.querySelector('p span');
		var counter = parseInt(counterElement.textContent, 10);

		++counter;

		counterElement.textContent = counter;
	}

	function addItem(element) {

		document.querySelector('ul').appendChild(element);

		incrementCounter();

	}

	function decrementCounter() {
		var counterElement = document.querySelector('p span');
		var counter = parseInt(counterElement.textContent, 10);

		--counter;

		counterElement.textContent = counter;
	}

	function markAsDone() {
		var counter = this.parentElement.parentElement;
		if (counter.className === 'done') {
			counter.className = '';
			incrementCounter();
		}
		else {
			counter.className = 'done';
			decrementCounter();
		}
	}

	function removeItem() {
		if (this.parentElement.parentElement.className !== 'done') {
			decrementCounter();
		}
		this.parentElement.parentElement.parentElement.removeChild(this.parentElement.parentElement);

	}

	function getNewItemText() {
		return document.querySelector('input').value;
	}

	//bindAction pro kazdej cudlik
	function bindAction(selector, callback) {
		var buttons = document.querySelectorAll(selector);

		var length = buttons.length;
		if (length === 0) {
			throw new Error('No button found for given selector "' + selector + '"')
		}

		for (var buttonIndex = 0;buttonIndex < length; ++buttonIndex) {
			buttons[buttonIndex].addEventListener('click', callback);
		}
	}
	return {
		createItem: createItem,
		addItem: addItem,
		markAsDone: markAsDone,
		removeItem: removeItem,
		getNewItemText: getNewItemText,
		bindAction: bindAction
	}
})();

if (typeof module !== 'undefined') {
	module.exports = TODO;
}
