/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function addItem', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.addItem === 'function');
	});

	it('should add item to empty list', function () {
		document.body.innerHTML = '<body><p><span>0</span></p><ul></ul></body>';

		TODO.addItem(document.createElement('div'));

		var resultElement = document.querySelectorAll('ul div');

		resultElement.should.have.lengthOf(1);

		var resultCounter = document.querySelector('p span');
		resultCounter.textContent.should.equal('1');
	})

});