/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function removeItem', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.removeItem === 'function');
	});

	it('should remove item', function () {
		document.body.innerHTML = '<body>' +
			'<p>' +
			'<span>1</span>' +
			'</p>' +
			'<ul>' +
			'<li>Připravit' +
			'<div class="button">' +
			'<button>✔</button> <button>X</button>' +
			'</div>' +
			'</li>' +
			'</ul>' +
			'</body>';

		var doneButton = document.querySelector('button');

		TODO.removeItem.call(doneButton);

		var resultElement = document.querySelectorAll('li');
		var resultCounter = document.querySelector('p span');

		resultElement.should.have.lengthOf(0);
		resultCounter.textContent.should.equal('0');
	});

	it('should remove done item', function () {
		document.body.innerHTML = '<body>' +
			'<p>' +
			'<span>0</span>' +
			'</p>' +
			'<ul>' +
			'<li class="done">Připravit' +
			'<div class="button">' +
			'<button>✔</button> <button>X</button>' +
			'</div>' +
			'</li>' +
			'</ul>' +
			'</body>';

		var doneButton = document.querySelector('button');

		TODO.removeItem.call(doneButton);

		var resultElement = document.querySelectorAll('li');
		var resultCounter = document.querySelector('p span');

		resultElement.should.have.lengthOf(0);
		resultCounter.textContent.should.equal('0');
	});

});