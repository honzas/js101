/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function createItem', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.createItem === 'function');
	});

	it('should return new item with correct text', function () {

		var result = TODO.createItem('pridana vec');

		should.exist(result);
		result.textContent.should.containEql('pridana vec');
		var addedItem = result.querySelector('div');
		should.exist(addedItem);
		addedItem.should.be.an.instanceOf(Object);
		addedItem.className.should.equal('button');
		var buttons = addedItem.querySelectorAll('button');
		buttons.should.have.lengthOf(2);
		var buttonOkText = '✔';
		var buttonCancelText = 'X';
		buttons[0].textContent.should.equal(buttonOkText);
		buttons[1].textContent.should.equal(buttonCancelText);
	});

});