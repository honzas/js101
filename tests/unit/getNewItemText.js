/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function getNewItemText', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.getNewItemText === 'function');
	});

	it('should get text from text input', function () {
		document.body.innerHTML = '<body><input type="text" name="name" placeholder="Nový úkol" value="test"></body>';

		TODO.getNewItemText().should.containEql('test');
	});

});