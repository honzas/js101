/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function markAsDone', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.markAsDone === 'function');
	});

	it('should mark not done item as done', function () {
		document.body.innerHTML = '<body>' +
			'<p>' +
			'<span>1</span>' +
			'</p>' +
			'<ul>' +
			'<li>Připravit' +
			'<div class="button">' +
			'<button>✔</button> <button>X</button>' +
			'</div>' +
			'</li>' +
			'</ul>' +
			'</body>';

		var doneButton = document.querySelector('button');

		TODO.markAsDone.call(doneButton);

		var resultElement = document.querySelector('li');
		var resultCounter = document.querySelector('p span');

		resultElement.className.should.equal('done');
		resultCounter.textContent.should.equal('0');
	});

	it('should mark done item as not done', function () {
		document.body.innerHTML = '<body>' +
			'<p>' +
			'<span>0</span>' +
			'</p>' +
			'<ul>' +
			'<li class="done">Připravit' +
			'<div class="button">' +
			'<button>✔</button> <button>X</button>' +
			'</div>' +
			'</li>' +
			'</ul>' +
			'</body>';

		var doneButton = document.querySelector('button');

		TODO.markAsDone.call(doneButton);

		var resultElement = document.querySelector('li');
		var resultCounter = document.querySelector('p span');

		resultElement.className.should.equal('');
		resultCounter.textContent.should.equal('1');
	});

});