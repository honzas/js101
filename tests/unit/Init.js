/**
 * Created by jan on 4/29/16.
 */
var assert = require('assert');

var should = require('should');

var jsdom = require('mocha-jsdom');

var leche = require('leche');

var withData = leche.withData;

var TODO = require('../../todo.js');


describe('function bindAction', function () {

	jsdom();

	it('should exist', function () {
		assert.ok(typeof TODO.bindAction === 'function');
	});

	it('should complain, when there is no button found', function () {
		document.body.innerHTML = '<body><ul></ul></body>';

		TODO.bindAction.bind(null, 'button').should.throw('No button found for given selector "button"');
	});

	it('should bind callback to button with given selector', function () {
		document.body.innerHTML = '<body><button></button></body>';
		TODO.bindAction('button', function () {
			this.textContent = 'test';
		});

		var button = document.querySelector('button');

		button.click.apply(button);

		button.textContent.should.containEql('test');
	});
});