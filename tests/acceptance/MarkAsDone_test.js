
Feature('MarkAsDone');

Scenario('Mark first item as done and then as undone', (I) => {

	I.amOnPage('/todo.html');
	I.see('3 nehotových');
	I.click('✔', 'li:nth-child(1)');
	I.see('Připravit', 'li.done');
	I.see('2 nehotových');

	I.click('✔', 'li:nth-child(1)');
	I.dontSeeElement('li.done');
	I.see('Připravit', 'li');
	I.see('3 nehotových');
});

Scenario('Mark second item as done', (I) => {

	I.amOnPage('/todo.html');
	I.click('✔', 'li:nth-child(2)');
	I.see('Vysvětlit', 'li.done');
	I.see('2 nehotových');

});

Scenario('mark as done item which has been dynamically added', (I) => {

	I.amOnPage('/todo.html');

	I.fillField('name', 'new item added');
	I.click('Vytvořit');

	I.click('✔', 'li:nth-child(4)');
	I.see('new item added', 'li.done');
	I.see('3 nehotových');

});
