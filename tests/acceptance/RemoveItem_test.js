
Feature('RemoveItem');

Scenario('Remove item', (I) => {

	I.amOnPage('/todo.html');
	I.click('X', 'li:nth-child(1)');
	I.dontSee('Připravit');
	I.seeNumberOfElements('li', 2);
	I.see('2 nehotových');

});

Scenario('Remove item which has been dynamically added', (I) => {

	I.amOnPage('/todo.html');

	I.fillField('name', 'new item added');
	I.click('Vytvořit');

	I.click('X', 'li:nth-child(4)');
	I.dontSee('new item added', 'li');
	I.seeNumberOfElements('li', 3);
	I.see('3 nehotových');

});

Scenario('Remove done item', (I) => {

	I.amOnPage('/todo.html');
	I.click('✔', 'li:nth-child(1)');
	I.click('X', 'li:nth-child(1)');
	I.dontSee('Připravit');
	I.seeNumberOfElements('li', 2);
	I.see('2 nehotových');

});