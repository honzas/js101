
Feature('AddItem');

	Scenario('add new item to non empty list', (I) => {
		I.amOnPage('/todo.html');
		I.dontSee('new item added');
		I.see('3 nehotových');
		I.fillField('name', 'new item added');
		I.click('Vytvořit');
		I.see('new item added');
		I.see('4 nehotových');
	});
