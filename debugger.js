
// Revealing module pattern (RMP)

var module = (function () {

  function privateFnc() {
    return Array.prototype.slice.call(arguments).map(function (arg) {
      return arg
        .toString()
        .replace(/(\[|\])/g, '|')
        .replace(/function [^\(]*\(([^\)]*)\)/g, '($1) =>');
    }).join('<br>');
  }

  var privateVar = [{
    obj: true
  }, [
    'array'
  ], 'string', 10, function square() {
    return int * int;
  }];

  return {
    method: function () {
      return privateFnc.apply(this, privateVar);
    }
  };

}());

document.getElementById('output').innerHTML = module.method();
